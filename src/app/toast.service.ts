import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  toasts: any[] = [];

  static ID = Symbol();
  constructor() { }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  success(text: string) {
    this.show(text, {
      classname: 'bg-success text-light',
      headertext: text,
      delay: 6000,
      autohide: true
    });
  }

  error(text: string) {
    this.show(text, {
      classname: 'bg-danger text-light',
      headertext: text,
      delay: 6000,
      autohide: true
    });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
