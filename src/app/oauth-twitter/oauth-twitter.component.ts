import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-oauth-twitter',
  templateUrl: './oauth-twitter.component.html',
  styleUrls: ['./oauth-twitter.component.scss']
})
export class OauthTwitterComponent implements OnInit {
  authenticating: any;
  redirect_uri: string;
  apiUrl;
  mode;
  result;

  callback = (e) => {
    if (e && e["origin"] == document.location.origin && e["data"] && e["data"]["type"] == `oauth:twitter`) {
      this.authenticating = true;
      this.api.get({
        endpoint: this.apiUrl,
        params: {
          oauth_token: e["data"]["args"]["oauth_token"],
          oauth_verifier: e["data"]["args"]["oauth_verifier"],
        },
      }).subscribe((response: any) => {
        this.result = response;
        window.removeEventListener('message', this.callback);
      }, err => {
        this.authenticating = false;
        window.removeEventListener('message', this.callback);
        this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      });
    }
  };

  constructor(
    private api: ApiService,
    private toast: ToastService,
    public config: ConfigService
  ) {
    const baseUrl = document.location.origin;
    this.redirect_uri = baseUrl.includes('localhost') ? baseUrl + '/assets/oauth/twitter/index.html' :
      baseUrl + '/xano-twitter-oauth/assets/oauth/twitter/index.html';
  }

  ngOnInit(): void {
  }

  initOAuth(mode, endpoint) {
    this.mode = mode;
    this.apiUrl = endpoint;

    let oauthWindow = window.open("", `twitterOauth`, `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=600,height=660,left=100,top=100`);

    window.removeEventListener('message', this.callback);
    window.addEventListener('message', this.callback);

    this.api.get({
      endpoint: `${this.config.xanoApiUrl}/oauth/twitter/request_token`,
      params: {
        redirect_uri: this.redirect_uri
      }
    }).subscribe((response: any) => {
      oauthWindow.location.href = response.authUrl;
    }, err => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
      oauthWindow.close();
    });
  }

  continueOAuthTwitter() {
    this.initOAuth('continue', `${this.config.xanoApiUrl}/oauth/twitter/access_token`);
  }

  logout() {
    this.result = false;
    this.mode = '';
  }

  getRedirectURI() {
    return this.redirect_uri;
  }
}
